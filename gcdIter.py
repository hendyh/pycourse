
def gcdIter(a, b):
    '''
    a, b: positive integers
    
    returns: a positive integer, the greatest common divisor of a & b.
    '''
    if a == 1 or b == 1:  
       return 1
    if a == b:
        return a
   
    test = min(a, b) 
       
    while test > 1:
        if a % test == 0 and b % test == 0:
            return test 
        else:
            test -= 1
            
    return 1
    
print(gcdIter(11,11))
    