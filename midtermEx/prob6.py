
def max_val(t): 
    """ t, tuple 
        Each element of t is either an int, a tuple, or a list
        No tuple or list is empty
        Returns the maximum int in t or (recursively) in an element of t """ 
    
    myMax = 0
    for element in t:
        try:
            if element > myMax:
                myMax = element
        except:
            if max_val(element) > myMax:
                myMax = max_val(element)
    return myMax

#print(max_val((5, (1, 2), [[1], [2]])))
print(max_val((5, (1, 2), [[1], [9]])))