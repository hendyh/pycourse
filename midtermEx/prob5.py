    
def uniqueValues(aDict):
    '''
    aDict: a dictionary
    '''
    # Your code here
    myValues = [] 
    myDict = {}
    
    for key, value in aDict.items():
        if value not in myValues:
            myDict[key] = value
            myValues.append(value)
    if len(myValues) == 1:
        return []
        
    if myDict == {}:
        return []
    return sorted(myDict.keys())
    
"""
Test: uniqueValues({1: 1})  = [1]
Test: uniqueValues({1: 1, 2: 1, 3: 3})  = [3]
uniqueValues({1: 1, 3: 2, 6: 0, 7: 0, 8: 4, 10: 0}) = [1, 3, 8]
Test: uniqueValues({8: 3, 1: 3, 2: 2}) = 2
Test: uniqueValues({0: 3, 1: 2, 2: 3, 3: 1, 4: 0, 6: 0, 7: 4, 8: 2, 9: 7, 10: 0}) = [3, 7, 9]

"""
myDict  = {5: 1, 7: 1}  # output is  []
print(uniqueValues(myDict))
 
        
   
    
    
            
    
        
        
        
     
    
    
      
aDict = {'a': 1, 'c' : 2, 'd' : 3, 'b' : 5 }