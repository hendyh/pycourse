def f(highest_score, next_highest_score):
    return highest_score + next_highest_score 
    

def char_score(letter):
    alphabet = 'a b c d e f g h i j k l m n o p q r s t u v w x y z'
    letters  = alphabet.split()
    for index in range(len(letters)):
        if letter == letters[index]:
            return index + 1 
    return index

def score(word, f):
    word = word.lower()
    word_score = []
    for index in range(len(word)):
         word_score.append(index * char_score(word[index])) 
    word_score = sorted(word_score)       
    return f(word_score.pop(), word_score.pop())

print(score('zzz', f))        
        

            