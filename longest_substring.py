"""
print the longest substring of s in which the letters 
occur in alphabetical order. For example, if s = 'azcbobobegghakl', then your 
program should print:
Longest substring in alphabetical order is: beggh
"""

s = "azcbobobegghakl"
largest_size = 0
pos = 0

for char in range(len(s)):
    char_count = 0
    next_char = char + 1
    
    while next_char + char_count <= len(s) - 1:
        if s[next_char + char_count] >= s[char + char_count]:
            char_count += 1
        else:
            break

    if char_count + 1> largest_size:
        largest_size = char_count + 1 
        pos = char 

print("Longest substring in alphabetical order is: " + s[pos:pos + largest_size])
