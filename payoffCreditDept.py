
balance = 3329
annualInterestRate = 0.2

minimumFixedPayment = 0.0

# Copy balance into myBalance
myBalance = balance

# Monthly interest rate
monthlyInterestRate = annualInterestRate/12.0

# Loop through calculating if monthly payment will pay off in 12 months
while myBalance > 0.0:

    # Increment minimum monthly fixed payment by $10 each time through loop
    minimumFixedPayment += 10.0

    for month in range (0, 12):

        # Apply payment to balance
        myBalance -= minimumFixedPayment

        # Add interest to balance
        myBalance += (myBalance * monthlyInterestRate)

    if myBalance > 0:
        # Reset balance each time through loop
        myBalance = balance

# Print the lowest monthly payment that will pay off the debt in 12 months
print("Lowest Payment  {}".format(minimumFixedPayment))
