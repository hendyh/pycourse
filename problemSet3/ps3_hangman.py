# Hangman game
# -----------------------------------
# Helper code
# You don't need to understand this helper code,
# but you will have to know how to use the functions
# (so be sure to read the docstrings!)

import random

WORDLIST_FILENAME = "/home/ec2-user/environment/pyCourse/problemSet3/words.txt"

def loadWords():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = line.split()
    print("  ", len(wordlist), "words loaded.")
    return wordlist

def chooseWord(wordlist):
    """
    wordlist (list): list of words (strings)

    Returns a word from wordlist at random
    """
    return random.choice(wordlist)

# end of helper code
# -----------------------------------

# Load the list of words into the variable wordlist
# so that it can be accessed from anywhere in the program
wordlist = loadWords()

def isWordGuessed(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: boolean, True if all the letters of secretWord are in lettersGuessed;
      False otherwise
    '''
    # FILL IN YOUR CODE HERE...
    for i in range(len(secretWord) ):
        if secretWord[i] not in lettersGuessed:
            return False
    return True


def getGuessedWord(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters and underscores that represents
      what letters in secretWord have been guessed so far.
    '''
    # FILL IN YOUR CODE HERE...
    show_guess = ""
    for i in range(len(secretWord) ):
        if secretWord[i] not in lettersGuessed:
            show_guess += ' _ '
        else:
            show_guess += secretWord[i]
    return show_guess


def getAvailableLetters(lettersGuessed):
    '''
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters that represents what letters have not
      yet been guessed.
    '''
    # FILL IN YOUR CODE HERE...
    if lettersGuessed == []:
        return 'abcdefghijklmnopqustuvwxyz'
    else:
        lettersGuessed = uniqueList(lettersGuessed)
        alphabet = "a b c d e f g h i j k l m n o p q r s t u v w x y z"
        letters_available = alphabet.split() 
        for char in lettersGuessed:
            letters_available.remove(char)
        return ''.join(letters_available)
        
        
def uniqueList(l):
    x = []
    for a in l:
        if a not in x:
            x.append(a)
    return x
    

def hangman(secretWord):
    '''
    secretWord: string, the secret word to guess.

    Starts up an interactive game of Hangman.

    * At the start of the game, let the user know how many 
      letters the secretWord contains.

    * Ask the user to supply one guess (i.e. letter) per round.

    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computers word.

    * After each round, you should also display to the user the 
      partially guessed word so far, as well as letters that the 
      user has not yet guessed.

    Follows the other limitations detailed in the problem write-up.
    '''

    mistakesMade = 0
    lettersGuessed = []
    print("Welcome to the game, Hangman!")
    print("I am thinking of a word that is {} letters long.".format(len(secretWord)))
    while mistakesMade < 8:
        print("-------------") 
        print("You have {} guesses left.".format(8-mistakesMade))
        print("Available letters: {}".format(getAvailableLetters(lettersGuessed)))
        l = input('Please guess a letter: ')
        if l not in getAvailableLetters(lettersGuessed):
            print("Oops! You've already guessed that letter: {}".format
                (getGuessedWord(secretWord, lettersGuessed)))
        
        elif secretWord.find(l) == -1 :  #  letter not in secretWord
            lettersGuessed.append(l)
            mistakesMade += 1
            print("Oops! That letter is not in my word: {}".format
                (getGuessedWord(secretWord, lettersGuessed)))
            
        elif l in secretWord:
            lettersGuessed.append(l)
            print("Good guess: {}".format(getGuessedWord(secretWord, lettersGuessed)))
                
            if isWordGuessed(secretWord, lettersGuessed):
                print("------------")
                print("Congratulations, you won!")
                return
            
    print("-----------")
    print("Sorry, you ran out of guesses. The word was {}.".format(secretWord))
        
secretWord = chooseWord(wordlist).lower()
hangman(secretWord)
