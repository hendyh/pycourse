
lettersGuessed = ['e', 'i', 'k', 'p', 'r', 's']
alphabet = "a b c d e f g h i j k l m n o p q r s t u v w x y z"
letters_available = alphabet.split() 


def getAvailableLetters(lettersGuessed):
    '''
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters that represents what letters have not
      yet been guessed.
    '''
    # FILL IN YOUR CODE HERE...
    if lettersGuessed == []:
        return '' 
        
    for char in lettersGuessed:
        letters_available.remove(char)
    return ''.join(letters_available)
    
    
isLetterAlreadyTried(letter, lettersGuessed):
    available_letters = availableLetters(lettersGuessed)
    if letter in available_letters: 
        return True
    return False
    
print(getAvailableLetters(lettersGuessed))

