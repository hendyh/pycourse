
secretWord = 'carrot' 
#lettersGuessed = ['e', 'i', 'k', 'p', 'r', 's']
lettersGuessed =   ['o', 'c', 'h', 'r', 'p', 'z', 'j', 'e', 'f', 'a']

def isWordGuessed(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: boolean, True if all the letters of secretWord are in lettersGuessed;
      False otherwise
    '''
    # FILL IN YOUR CODE HERE...
    for i in range(len(secretWord) ):
      if secretWord[i] not in lettersGuessed:
        return False
    return True
    
print(isWordGuessed(secretWord, lettersGuessed))    

