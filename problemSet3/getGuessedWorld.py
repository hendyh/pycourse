
secretWord = 'apple' 
lettersGuessed = ['e', 'i', 'k', 'p', 'r', 's']

def getGuessedWord(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters and underscores that represents
    what letters in secretWord have been guessed so far.
    '''
    # FILL IN YOUR CODE HERE...
    show_guess = ""
    for i in range(len(secretWord) ):
        if secretWord[i] not in lettersGuessed:
            show_guess += ' _ '
        else:
            show_guess += secretWord[i]
    return show_guess
    
print(getGuessedWord(secretWord, lettersGuessed))    

