annualInterestRate = 0.2
monthlyPaymentRate = 0.04
balance = 42 
month =  0

def creditBal(balance, monthlyPaymentRate):
    global month
    global new_balance
    month += 1  
    if month > 12:
        return "Remaining balance: {0:.2f}".format(new_balance)
    
    payment  = monthlyPaymentRate * balance
    interest = (balance - payment) * annualInterestRate / 12
    new_balance = balance - payment + interest
    return creditBal(new_balance, monthlyPaymentRate)

print(creditBal(balance, monthlyPaymentRate))

    