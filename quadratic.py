import math

def evalQuadratic(a, b, c):
    '''
    a, b, c: numerical values for the coefficients of a quadratic equation
    x: numerical value at which to evaluate the quadratic.
    '''
    num = float(-b + math.sqrt(b**2 - 4*a*c) )
    x = num / (2 * a)  
    return x

print(evalQuadratic(3, -5, -7))
