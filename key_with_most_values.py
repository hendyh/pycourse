"""
This time, write a procedure, called biggest, which returns the key corresponding
to the entry with the largest number of values associated with it. If there is 
more than one such entry, return any one of the matching keys.

Example usage:
>>> biggest(animals)
'd'
"""
animals = { 'a': ['aardvark'], 'b': ['baboon'], 'c': ['coati']}
animals['d'] = ['donkey']
animals['d'].append('dog')
animals['d'].append('dingo')

def biggest(aDict):
    '''
    aDict: A dictionary, where all the values are lists.

    returns: int, how many values are in the dictionary.
    '''
    # Your Code Here
    values = [] 
    for animal in aDict:
        values.append(len(aDict[animal]))
    largest = max(values)
    
    for animal in aDict:
        if len(aDict[animal]) == largest:
            return animal
    
print(biggest(animals))
    