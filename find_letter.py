import from

def isIn(char, aStr):
    
    if aStr == '':
        return False
    
    lo = 0
    hi = len(aStr) - 1  
    middle_pos = int(((hi - lo) / 2))
    middle_char = aStr[middle_pos]
    
    if middle_char == char:
        return True 
        
    if len(aStr) < 2: 
        return False
        
    if middle_char < char:
        new_string = aStr[middle_pos+1:hi + 1]
        return isIn(char, new_string)
        
    if middle_char > char:
        new_string = aStr[lo:middle_pos+1]
        return isIn(char, new_string)

print(isIn('z', ''))    
    