high = 100
low = 0
guess = 50 

print("Please think of a number between 0 and 100!")
while True:
    print('Is your secret number ' + str(guess) + '?')
    answer = input("Enter 'h' to indicate the guess is too high. " +
                "Enter 'l' to indicate the guess is too low. " +
                "Enter 'c' to indicate I guessed correctly. ")
    
    if answer == 'h':
        high = guess
        guess = int(low + abs((high - low)) / 2) 
        
    elif answer == 'l':
        low =  int(guess) 
        guess = int((low + (abs(high - low))/2 ))
        
    elif answer == 'c':
        print("Game over, number is: " + str(guess))
        break
    
    elif answer == 'x':
        print("Ok have a good day, thanks for playing with me!")
        break 
    
    else:
        print("Invalid input, try again")
        